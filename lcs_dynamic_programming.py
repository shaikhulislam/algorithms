# longest common subsequence
# using dynamic programming

import string, random
import numpy as np
def random_chars(n=1, chars=string.ascii_uppercase):
    for i in range(n):
        yield random.choice(chars)
# a and b are lists
def lcs_dynamic_programming(a, b):
	matrix = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
	for i, x in enumerate(a):
		for j, y in enumerate(b):
			if x == y:
				matrix[i+1][j+1] = matrix[i][j] + 1
			else:
				matrix[i+1][j+1] = max(matrix[i+1][j], matrix[i][j+1])
	# read the subsequence out from the matrix
	result = []
	x, y = len(a), len(b)
	while x > 0 and y > 0:
		if matrix[x][y] == matrix[x-1][y]:
			x -= 1
		elif matrix[x][y] == matrix[x][y-1]:
			y -= 1
		else:
			result = [a[x-1]] + result
			x -= 1
			y -= 1
	return result, matrix

def mode(lst):
	return max(set(lst), key=lst.count)

def run_sims(num_sims=1, s1_len=2, s2_len=2):
	ars = []
	for sim in range(num_sims):
		alist = [c for c in random_chars(n=s1_len, chars='ATCG')]
		blist = [c for c in random_chars(n=s2_len, chars='ATCG')]
		res, matrix = lcs_dynamic_programming(alist, blist)
		ars.append(matrix)
	return ars

if __name__ == '__main__':

	#alist = [c for c in random_chars(n=10, chars='ATCG')]
	#blist = [c for c in random_chars(n=10, chars='ATCG')]
	m3d = run_sims(20, 5, 5)
	print type(m3d)
	mmode = mode(m3d)
	print mmode
	'''
	print 'alist:', alist
	print 'blist:', blist
	r, m = lcs_dynamic_programming(alist, blist)
	print 'result:', r
	print 'matrix:'
	print np.array(m)
	'''