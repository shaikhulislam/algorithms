# anagrams

def init_words(filename):
	words = {}
	with open(filename) as f:
		for line in f.readlines():
			word = line.strip()
			words[word] = 1
	return words

def init_anagram_dict(words):
	anagram_dict = {}
	for word in words:
		sorted_word = ''.join(sorted(list(word)))
		if sorted_word not in anagram_dict:
			anagram_dict[sorted_word] = []
		anagram_dict[sorted_word].append(word)
	return anagram_dict

def is_anagram_recursive(soFar, rest, lexicon, orig):
	if rest == []:
		word = ''.join(soFar)
		if word in lexicon and word != orig:
			return True
	else:
		for i in range(len(rest)):
			if is_anagram_recursive(soFar+[rest[i]], rest[:i]+rest[i+1:], lexicon, orig):
				return True
	return False

def is_anagram_from_dict(word, anagram_dict):
	key = ''.join(sorted(list(word)))
	if key in anagram_dict:
		values = anagram_dict[key]
		# >= 2 garauntees that 'word' is not the only value
		if len(values) >= 2:
			return True
	return False

def find_anagrams(word, anagram_dict):
	key = ''.join(sorted(list(word)))
	if key in anagram_dict:
		return set(anagram_dict[key]).difference(set([word]))
	return set([])

def test_dict(word_dict):
	ad = init_anagram_dict(word_dict.keys())
	anagrams = []
	for word in word_dict:
		if is_anagram_from_dict(word[:], ad):
			anagrams.append(word)
	return anagrams

def test_rec(word_dict):
	anagrams = []
	for word in word_dict:
		print word
		if is_anagram_recursive([], word[:], word_dict, word[:]):
			anagrams.append(word)
	return anagrams

word_dict = init_words('dict.txt')
anagrams_from_dict = test_dict(word_dict)
print anagrams_from_dict[:10]
print len(anagrams_from_dict), 'anagrams from dict'
#anagrams_from_rec = test_rec(word_dict)
#print len(anagrams_from_rec), 'anagrams from recursive'
print is_anagram_recursive([], list('caned'), word_dict, 'caned'[:])
print find_anagrams('caned', init_anagram_dict(word_dict.keys()))
